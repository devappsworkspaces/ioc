﻿namespace NinjectUI.Models
{
    public interface IOrderService
    {
        string CreateOrder();
    }
}
