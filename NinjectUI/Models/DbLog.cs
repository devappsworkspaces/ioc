﻿namespace NinjectUI.Models
{
    using System;

    public class DbLog : ILog
    {
        public Guid UniqueId { get; set; }

        public DbLog()
        {
            UniqueId = Guid.NewGuid();
        }

        public string Create()
        {
            return "Log created to db";
        }
    }
}
