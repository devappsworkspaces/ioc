﻿namespace NinjectUI.Models
{
    using System;

    public class FileLog : ILog
    {
        public Guid UniqueId { get; set; }

        public FileLog()
        {
            UniqueId = Guid.NewGuid();
        }

        public string Create()
        {
            return "Log created to file";
        }
    }
}
