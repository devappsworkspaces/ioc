﻿namespace NinjectUI.Models
{
    using System;

    public interface ILog
    {
        Guid UniqueId { get; set; }
        string Create();
    }
}
