﻿namespace SLP
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            ServiceLocator.Register<ILog>(new FileLog());

            var orderService = new OrderService();
            orderService.CreateOrder();

            Console.ReadLine();
        }
    }
}
