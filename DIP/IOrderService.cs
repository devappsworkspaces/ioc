﻿namespace DIP
{
    public interface IOrderService
    {
        void CreateOrder();
    }
}
