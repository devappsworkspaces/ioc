﻿namespace DIP
{
    public interface ILog
    {
        void Create();
    }
}
